function baseUrl(segment){
    pathArray = window.location.pathname.split( '/' );
    indexOfSegment = pathArray.indexOf(segment);
    return window.location.origin + pathArray.slice(0,indexOfSegment).join('/') + '/';
}

$(function () {
    'use strict';
    var url = baseUrl('admin') + 'admin/file/upload';
    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        autoUpload: false,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        maxFileSize: 5000000,
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewMaxWidth: 100,
        previewMaxHeight: 100,
        previewCrop: true,
        filesContainer: $('table.files'),
        uploadTemplateId: null,
        downloadTemplateId: null,
        uploadTemplate: function (o) {
            var rows = $();
            $.each(o.files, function (index, file) {
                var row = $('<tr class="template-upload fade">' +
                    '<td><span class="preview"></span></td>' +
                    '<td><p class="name"></p>' +
                    '<div class="error"></div>' +
                    '</td>' +
                    '<td><textarea name="description[]" class="image-description small-text"></textarea></td>' +
                    '<td><p class="size"></p>' +
                    '<div class="progress"></div>' +
                    '</td>' +
                    '<td>' +
                    (!index && !o.options.autoUpload ?
                        '<button class="start button interface" disabled>Загрузить</button>' : '') +
                    (!index ? '<button class="cancel button interface">Отмена</button>' : '') +
                    '</td>' +
                    '</tr>');
                row.find('.name').text(file.name);
                row.find('.size').text(o.formatFileSize(file.size));
                if (file.error) {
                    row.find('.error').text(file.error);
                }
                rows = rows.add(row);
            });
            return rows;
        },
        downloadTemplate: function (o) {
            var rows = $();
            $.each(o.files, function (index, file) {
                var row = $('<tr class="template-download fade">' +
                    '<td><span class="preview"></span></td>' +
                    '<td><p class="name"></p>' +
                    (file.error ? '<div class="error"></div>' : '') +
                    '</td>' +
                    '<td><p class="description"></p></td>' +
                    '<td><span class="size"></span></td>' +
                    '<td><button class="delete button interface">Удалить</button></td>' +
                    '</tr>');
                row.find('.size').text(o.formatFileSize(file.size));
                if (file.error) {
                    row.find('.name').text(file.name);
                    row.find('.error').text(file.error);
                } else {
                    row.find('.name').append($('<a></a>').text(file.name));
                    row.find('.description').text(file.description);
                    if (file.thumbnailUrl) {
                        row.find('.preview').append(
                            $('<a></a>').append(
                                $('<img>').prop('src', file.thumbnailUrl)
                            )
                        );
                    }
                    row.find('a')
                        .attr('data-gallery', '')
                        .prop('href', file.url);
                    row.find('button.delete')
                        .attr('data-type', file.delete_type)
                        .attr('data-url', file.delete_url);
                }
                rows = rows.add(row);
            });
            return rows;
        }
    }).on('fileuploadsubmit', function (e, data) {
            data.formData = data.context.find(':input').serializeArray();
        }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});

$(function () {
    CKEDITOR.replace('redactor');

});

$(function () {
    $(document).ready(function()
    {
        $("#preview").uploadFile({
            url: baseUrl('admin') + "admin/preview/upload",
            allowedTypes:"png,gif,jpg,jpeg",
            multiple: false,
            returnType: 'json',
            fileName: "preview",
            onSuccess:function(files,data,xhr) {
                if (!data.error)
                {
                    $('#preview-preview').html($('<img>', { src : data.url, width : 80 }));
                    $('input[name="Project[preview]"]').val(data.id);
                }

            }
        });
    });
});

$(function() {
    $("#sortable").sortable({
        update: function (event, ui) {
            var data = $(this).sortable('serialize');
            $.ajax({
                data: data,
                type: 'POST',
                url: '/admin/image/sort'
            });
        }
    });
});

$(function(){
    $('.image-delete').click(function(event) {
        event.preventDefault();
        if (confirm("Удалить изображение?"))
        {
            var item = $(this).data('item');
            var element = $(this).parent();
            $.ajax({
                url : '/admin/image/delete',
                type : 'post',
                data : { id :  item},
                success: function(res) {
                    element.remove();
                }
            })
        }

    })
});

$(function(){
    $('#delete-preview').click(function(event) {
        event.preventDefault();
        if (confirm("Удалить превью?"))
        {
            var item = $(this).data('item');
            var element = $('#preview-preview');
            $('input[name="Project[preview]"]').val('');
            $.ajax({
                url : baseUrl('admin') + 'admin/preview/delete',
                type : 'post',
                data : { id :  item},
                success: function(res) {
                    element.empty();
                }
            })
        }

    })
});
$(function(){
    $(document).on('click', '.image-description', function(event) {
        event.preventDefault();
        var container = $(this);
        var clone = $(this).clone();

        if (!container.hasClass('edit'))
        {
            container.addClass('edit');

            var text = container.text();
            var item = container.data('item');

            var editable = $('<textarea></textarea>', { class: 'small-text',html: text });

            var cancel = $('<button></button>', {
                class: 'interface button',
                text: 'Отмена',
                click: function(evt) {
                    evt.preventDefault();
                    container.replaceWith(clone);
                }
            });

            var submit = $('<button></button>', {
                class: 'interface button',
                text: 'Сохранить',
                click: function(evt) {
                    evt.preventDefault();
                    $.ajax({
                        url : baseUrl('admin') + 'admin/image/edit',
                        type : 'post',
                        data : { id : item, description : editable.val() },
                        success: function(res) {
                            container.html(res);
                            container.removeClass('edit');
                        }
                    });
                }
            });

            $(this).empty().append(editable, submit, cancel);
        }
    });
});