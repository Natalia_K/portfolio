$(function() {
    $("#sortable").sortable({
        update: function (event, ui) {
            var data = $(this).sortable('serialize');
            $.ajax({
                data: data,
                type: 'POST',
                url: 'http://hramov.loc/admin/project/sort'
            });
        }
    });
});

$(function(){
    $('.project-delete').click(function(event) {
        event.preventDefault();
        if (confirm("Удалить проект?"))
        {
            var item = $(this).data('item');
            var element = $(this).parent();
            $.ajax({
                url : '/admin/project/delete',
                type : 'post',
                data : { id :  item},
                success: function(res) {
                    element.remove();
                }
            })
        }

    })
});