jQuery(function($) {
    var navbar = $('.welcome-menu-navbar');
    if (navbar.is('nav'))  {
        var topPos = navbar.offset().top;

            $(window).scroll(function() {
                var top = $(document).scrollTop();
                if (top > topPos) navbar.addClass('sticky');
                else navbar.removeClass('sticky');
            });

            navbar.find('a').click(function(event) {
                event.preventDefault();
                $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top-60}, 800);
            });
    }

    $('#enter').click(function(event) {
        event.preventDefault();
        $('#overlay').modal('show');
    })
});