jQuery(function($) {
    $(document).ready(function() {
        $('.viewport').mCustomScrollbar();
        $('.list-item').eq(0).trigger('click');

        var slider = $('.bx-slider').bxSlider({
            pager : false,
            maxSlides: 4,
            minSlides: 1,
            moveSlides: 1,
            infiniteLoop: false,
            slideWidth: 250,
           /* onSlideNext : function(slideElement, oldIndex, newIndex){ slideElement.activateSlide();  },
            onSlidePrev : function(slideElement, oldIndex, newIndex){ /* slideElement.activateSlide() }, */
            nextSelector : '.list-controls-next',
            prevSelector : '.list-controls-prev',
            nextText : '',
            prevText : '',
            slideMargin: 10,
            hideControlOnEnd: false
        });
        $('.list-controls-next').click(function() {
            var next = $('.list-item.active').next();
            if (next.index() == -1)
                $(this).addClass('disabled');
            next.activateSlide();
        });
        $('.list-controls-prev').click(function() {
            var prev = $('.list-item.active').prev();
            if (prev.index() == -1)
                $(this).addClass('disabled');
            prev.activateSlide();
        });
        $(document).on('click', '.project-controls', function(event) {
            event.preventDefault();
            $('#overlay').projectOpen($(this).data('item'), 'slide');
            var slide = $('.list-item-details[data-item="'+$(this).data('item')+'"]').parent();
            slide.activateSlide();
            slider.goToSlide(slide.index());
        });

    });



    $(window).resize(function() {
        var overlay = $('#overlay');
        if (overlay.is(':visible')) {
            if ($(window).width() > 1200)  {
                var height = parseInt(overlay.find('.project-container').outerHeight()) - parseInt(overlay.find('.project-heading').outerHeight() + 40);
                $('#project-gallery, #project-details').height(height);
            }
        }
        $('#project-gallery, #project-details').mCustomScrollbar();
    });




    $.fn.activateSlide = function() {
        $(this).addClass('active').siblings().removeClass('active');
        $('#item-'+$(this).data('item')).fadeIn('fast').siblings('.viewport-slide').hide();
        $('#item-'+ $(this).data('item')).mCustomScrollbar().siblings('.viewport-slide').mCustomScrollbar('destroy');
    };

    $.fn.projectOpen = function(projectId, mode) {
        var overlay = $(this);
        $.ajax({
            url:(window.location.href.indexOf('\?') != -1 ? window.location.href.slice(0,window.location.href.indexOf('\?')) : window.location.href) + 'project',
            type: 'post',
            data: { project : projectId , ajax : 1},
            beforeSend: function() { overlay.addClass('loading');  if(!overlay.is(':visible')) overlay.fadeIn() },
            success: function(res) {
                overlay.html(res);
                overlay.find('canvas').each(function() {
                    createChart($(this).attr('id'), $(this).data('value'))
                });
                $(window).trigger('resize');
                overlay.removeClass('loading');
                if (mode != 'slide')
                    overlay.fadeIn('fast');

            }
        });
    };

    $(document).on('click', '.list-item-details', function(event) {
        event.preventDefault();
        $('#overlay').projectOpen($(this).data('item'));
    });

    $(document).on('click', '#show-overlay', function(event) {
        event.preventDefault();
        $('#overlay').projectOpen($('.list-item.active').data('item'));
    });

    $(document).on('mouseenter', '#show-overlay', function(event) {
        $('#viewport-overlay').css('visibility', 'visible');
    }).on('mouseout', '#show-overlay', function(event) {
        $('#viewport-overlay').css('visibility', 'hidden');
    });

    $(document).on('click', '#hide-overlay', function(event) {
        event.preventDefault();
        $('#overlay').fadeOut().empty();
    });

    $(document).on('click', '.list-item', function(e) { e.preventDefault(); $(this).activateSlide() });


    function createChart(selector, number) {
        var drawingCanvas = document.getElementById(selector);
        if(drawingCanvas && drawingCanvas.getContext) {
            var context = drawingCanvas.getContext('2d');
            context.fillStyle = "#b795ff";
            context.strokeStyle = "#b795ff";
            context.beginPath();
            context.arc(25,25,20,0,Math.PI*2,true);
            context.closePath();
            context.fill();

            context.fillStyle = "#702CFF";
            context.beginPath();
            context.moveTo(25, 25);
            var start=(Math.PI/180)*90-((Math.PI/180)*number*360/100)/2;
            context.arc(25,25,20,start,start+(Math.PI/180)*number*360/100,false);
            context.closePath();
            context.fill();

            context.fillStyle = "#FFFFFF";
            context.beginPath();
            context.arc(25,25,12,0,Math.PI*2,true);
            context.closePath();
            context.fill();

            context.fillStyle = "#000000";
            context.font = "9px Open Sans";
            context.textAlign = "center";
            context.textBaseline = 'middle';
            var x=drawingCanvas.width/2;
            var y=drawingCanvas.height/2;
            context.fillText(number+"%", x,y);
        }
    }




    $(document).mouseup(function(evt) {
        if ($('.project-container').has(evt.target).length === 0 && $('#overlay').is(':visible')) {
            $('#overlay').fadeOut().empty();
        }
    });


});





