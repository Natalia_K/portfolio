<?php
class UrlManager
{
    private $base;

    public function __construct(Array $arguments = array())
    {
        if (!empty($arguments)) {
            foreach ($arguments as $property => $argument) {
                $this->{$property} = $argument;
            }
        }
    }

    public function parseUrl()
    {
        $params = array();
        $uri = str_replace($this->base, '',$_SERVER['REQUEST_URI']);
        $parts = explode('?', $uri);
        if (!empty($parts[1]))
        {
            $pairs = explode('&', $parts[1]);
            foreach($pairs as $p)
            {
                $pt = explode('=', $p);
                $params[$pt[0]] = $pt[1];
            }

        }
        $segments = explode('/', $parts[0]);
        array_shift($segments);
        if (!empty($segments[0]))
        {
            if ($segments[0] == 'admin')
            {
                $_POST['target'] = 'admin';
                $_POST['action'] = (isset($segments[1]) ? $segments[1] : '') . (isset($segments[2]) ? ucfirst($segments[2]) : '');
                $_POST['params'] = isset($segments[3]) && is_numeric($segments[3]) ? array('id' => $segments[3]) : array();
            }
            else
            {
                $_POST['target'] = 'action';
                $_POST['action'] = (isset($segments[0]) ? $segments[0] : '');
                $_POST['params'] = isset($segments[1]) && is_numeric($segments[1]) ? array('id' => $segments[1]) : array();
            }
        }
        else
        {
            $_POST['target'] = 'action';
            $_POST['action'] = 'index';
            $_POST['params'] = array();
        }
        $_POST['params'] = array_merge($_POST['params'], $params);

    }

    public function createUrl($route, $params=array())
    {
        $q = array();
        if (!empty($params))
            foreach ($params as $key => $value)
            {
                $q[] = $key.'='.$value;
            }
        $query = join('&', $q);
        return $this->base.'/'.$route.(!empty($query) ? '?'.$query : '');
    }
}


