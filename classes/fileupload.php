<?php
require(dirname(__FILE__) .'/jqFileUpload/UploadHandler.php');

class CustomUploadHandler extends UploadHandler {

    protected function initialize() {
        parent::initialize();
        @session_start();
    }

    protected function handle_form_data($file, $index) {
        $file->description = @$_REQUEST['description'][$index];
    }

    protected function handle_file_upload($uploaded_file, $name, $size, $type, $error,
                                          $index = null, $content_range = null) {
        $file = parent::handle_file_upload(
            $uploaded_file, $name, $size, $type, $error, $index, $content_range
        );
        if (empty($file->error)) {
            $_SESSION['uploads'][$file->name] = array(
                'name' => $file->name,
                'size' => $file->size,
                'type' => $file->type,
                'description' => $file->description
            );
        }
        return $file;
    }

    public function delete($print_response = true) {
        $response = parent::delete(false);
        foreach ($response as $name => $deleted) {
            if ($deleted) {
               unset( $_SESSION['uploads'][$name]);
            }
        }
        return $this->generate_response($response, $print_response);
    }

}
