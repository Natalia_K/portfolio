<?php
require_once("authentication.php");
require_once("urlmanager.php");
require_once("fileupload.php");
require_once(dirname(__FILE__) . '/ActiveRecord/ActiveRecord.php');

class Application
{
    private $_config;
    private $_auth;
    private $_errors = array();
    private $_urlManager;

    public $name;
    public $regalia;
    public $phone;
    public $mail;
    public $skype;


    public function __construct()
    {
        $this->initialize();
    }

    protected function initialize()
    {
        date_default_timezone_set('Europe/Moscow');
        $configPath = dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'cfg.php';
        if (is_file($configPath))
            $this->_config = require($configPath);

        $this->_auth = new Authentication(array(
            '_login' => $this->_config['username'],
            '_password' => $this->_config['password'])
        );
        $conn = array(
            'connections' => $this->_config['db'],
            'active' => $this->_config['activeDbConnection']
        );
        ActiveRecord\Config::initialize(function($cfg) use ($conn) {
            $cfg->set_model_directory(dirname(__FILE__) . '/Models');
            $cfg->set_connections($conn['connections']);
            $cfg->set_default_connection($conn['active']);
        });

        $this->_urlManager = new UrlManager(array('base' => $this->_config['baseDirectory']));

        $this->name = $this->_config["settings"]["name"];
        $this->regalia = $this->_config["settings"]["regalia"];
        $this->skills = $this->_config["settings"]["skills"];
        $this->phone = $this->_config["settings"]["phone"];
        $this->mail = $this->_config["settings"]["mail"];
        $this->skype = $this->_config["settings"]["skype"];

    }

    /*
     * геттеры
     */
    public function getErrors()
    {
        return $this->_errors;
    }

    public function setError($message)
    {
        $this->_errors[] = $message;
    }

    public function getAuth()
    {
        return $this->_auth;
    }

    public function getUrlManager()
    {
        return $this->_urlManager;
    }

    public function redirect($dest, $params=array())
    {
        header("Location:".$this->_urlManager->createUrl($dest, $params));
    }

    public function baseUrl()
    {
        return $this->_config['baseUrl'];
    }

    public function checkAccess($redirect = true)
    {
        if ($this->getAuth()->isAuth())
            return true;
        else
        {
            if ($redirect)
                $this->redirect('login');
            else
                return false;
        }

    }
    /*
     * MVC на коленке, хех
     * отделяем представления от всего остального
     */
    public function render($view, $params = array())
    {
        if (!empty($params))
            foreach($params as $variable => $value)
                ${$variable} = $value;
        $app = $this;
        $viewPath = dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.$view.'.php';
        if (is_file($viewPath))
            include($viewPath);
        else
            $this->setError("Страница не существует");
    }

    /*
     * получение параметра по имени из GET и POST
     */
    public function getParam($name)
    {
        if (isset($_POST[$name]))
            return $_POST[$name];
        elseif(isset($_GET[$name]))
            return isset($_GET[$name]);
        else
            return null;
    }

    public function actionLogin()
    {
        if (!$this->checkAccess(false))
        {
            $auth = $this->_auth;
            $login = $this->getParam('login');
            $pass = $this->getParam('password');
            if (!empty($login) && !empty($pass))
            {
                if (!$auth->auth($login, $pass))
                    $this->setError('Логин или пароль не верны');
                else
                    $this->redirect('admin/project/list');
            }
            $this->render('login');
        }
        else
            $this->redirect('admin/project/list');
    }

    public function actionLogout()
    {
        $auth = $this->_auth;
        if ($auth->isAuth())
            $auth->out();
        $this->redirect("");
    }

    /*
     * снова MVC на коленке
     * вызываем экшен "контроллера"
     */
    public function callAction($name, $params = array(), $prefix = 'action')
    {
        $method = $prefix.ucfirst($name);
        if (method_exists($this, $method))
           $this->{$method}($params);
        else
           $this->setError('Действие не найдено');
    }

    /*
     * загрузки из jQuery File Upload:
     * вынимаем из сессии инфу о загруженных файлах и сохраняем в бд
     */

    protected function handleUploads()
    {
        $saved = array();
        if (!empty($_SESSION['uploads']))
        {
            $data = $_SESSION['uploads'];
            foreach($data as $key => $image)
            {
                $model = new Image($image);
                if ($model->is_invalid())
                    $this->setError($model->errors->full_messages());
                else
                {
                    if ($model->save())
                        $saved[] = $model->id;
                    else
                        $this->setError('Не удалось сохранить файл '.$model->name);
                }
                unset($_SESSION['uploads'][$key]);
            }
        }
        return $saved;
    }

    /*
     * морда
     * главная страница
     */
    public function actionIndex()
    {
        $projects = Project::find('all', array('order' => 'weight asc'));
        $this->render('main', array('data' => $projects));
    }

    /*
     * морда
     * страница проекта
     */
    public function actionProject($params)
    {
        $id = isset($_POST['project']) ? intval($_POST['project']) : (isset($params['id']) ? intval($params['id']) : null);
        if ($id)
        {
            $model = Project::find($id);
            if ($model)
            {
                $first = Project::first(array('select' => 'id', 'order' => 'weight asc'));
                $last = Project::first(array('select' => 'id', 'order' => 'weight desc'));
                $next = Project::first(array('select' => 'id', 'conditions' => array('weight > ?', $model->weight), 'order' => 'weight asc'));
                $prev = Project::first(array('select' => 'id', 'conditions' => array('weight < ?', $model->weight), 'order' => 'weight desc'));
                if ($next && $prev)
                {
                    if ($next->id == $last->id)
                        $next = $last;
                    if ($prev->id == $first->id)
                        $prev = $first;
                }
                $this->render('project', array('model' => $model, 'next' => $next ? $next->id : $last->id, 'prev' => $prev ? $prev->id : $first->id));
            }
            else
            {
                $this->setError('страница не найдена');
                echo json_encode($this->getErrors());
            }
        }
        else
        {
            $this->setError('страница не найдена');
            echo json_encode($this->getErrors());
        }
    }

    /*
     * админка
     * создание проекта
     */
    public function adminProjectCreate()
    {
        $this->checkAccess();
        $model = new Project();
        if (!empty($_POST['Project']))
        {
            $model->set_attributes($_POST['Project']);

            $last = Project::find('last');
            if ($last)
                $model->weight = $last->weight+1;
            else
                $model->weight = 0;

            if ($model->is_invalid())
                $this->setError($model->errors->full_messages());
            else
            {
                if ($model->save())
                {
                    $images = $this->handleUploads();
                    if (!empty($images))
                    {
                        $count = 0;
                        foreach($images as $image)
                        {
                            $gallery = new Gallery(array('project' => $model->id, 'image' => $image, 'weight' => $count));
                            if ($gallery->is_invalid())
                                $this->setError($gallery->errors->full_messages());
                            else
                                $gallery->save();
                            $count++;
                        }
                    }
                    $this->redirect('admin/project/list');
                }
            }
        }
        $this->render('projectForm', array('model' => $model));
    }

    /*
     * админка
     * редактирование проекта
     */
    public function adminProjectEdit($params)
    {
        $this->checkAccess();
        if (isset($params['id']))
        {
            $model = Project::find(intval($params['id']));
            if ($model)
            {
                if (isset($_POST['Project']))
                {
                    if (!empty($model->preview) && $model->preview != intval($_POST['Project']['preview']))
                        $this->adminPreviewDelete(intval($_POST['Project']['preview']));

                    $model->set_attributes($_POST['Project']);
                    if ($model->is_invalid())
                        $this->setError($model->errors->full_messages());
                    else
                    {
                        if ($model->save())
                        {
                            $images = $this->handleUploads();
                            if (!empty($images))
                            {
                                $count = 0;
                                foreach($images as $image)
                                {
                                    $gallery = new Gallery(array('project' => $model->id, 'image' => $image, 'weight' => $count));
                                    if ($gallery->is_invalid())
                                        $this->setError($gallery->errors->full_messages());
                                    else
                                        $gallery->save();
                                    $count++;
                                }
                            }
                            $this->redirect('admin/project/list');
                        }
                    }
                }
                $this->render('projectForm', array('model' => $model));
            }
            else
                $this->setError('Проект не существует');
        }
        else
            $this->setError('Не указан проект');

    }

    /*
     * админка
     * список проектов
     */
    public function adminProjectList()
    {
        $this->checkAccess();
        $projects = Project::find('all', array('order' => 'weight asc'));
        $this->render('projectList', array('data' => $projects));
    }

    /*
     * админка
     * сортировка проектов
     */

    public function adminProjectSort()
    {
        if (!empty($_POST['item']))
        {
            foreach ($_POST['item'] as  $weight => $id)
            {
                $item = Project::find($id);
                $item->update_attributes(array('weight' => $weight));
            }
        }
    }

    /*
     * админка
     * удалить проект
     * ajax only
     */

    public function adminProjectDelete()
    {
        $id = $this->getParam('id');
        if ($id)
        {
            $model = Project::find($id);
            if ($model)
            {
                if ($model->galleries)
                {
                    foreach($model->galleries as $rel)
                        $rel->delete();
                }
                $model->delete();
            }
        }
    }

    /*
     * админка
     * загрузка картинки-превью для проекта
     */
    public function adminPreviewUpload()
    {
        if(isset($_FILES["preview"]))
        {
            $ret = array();
            $output_dir = dirname(__FILE__) . '/../files/previews/';
            $output_url = $this->baseUrl().'/files/previews/';
            $error =$_FILES["preview"]["error"];
            if (!$error)
            {
                if(!is_array($_FILES["preview"]["name"]))
                {
                    $fileName = $_FILES["preview"]["name"];
                    if (move_uploaded_file($_FILES["preview"]["tmp_name"],$output_dir.$fileName))
                    {
                        $model = new Image();
                        $model->set_attributes(array(
                            'name' => $_FILES["preview"]["name"],
                            'type' => $_FILES["preview"]["type"],
                            'size' => $_FILES["preview"]["size"]
                        ));
                        if ($model->is_invalid())
                            $ret = array('error' => 'Файл не прошел валидацию');
                        else
                        {
                            $model->save();
                            $ret= array('url' => $output_url.$fileName, 'id' => $model->id) ;
                        }
                    }
                }
            }
            else
                $ret = array('error' => 'Не удалось загрузить файл');
            echo json_encode($ret);
        }
    }
    /*
    * админка
    * удаление превью проекта
    */
    public function adminPreviewDelete($id=null)
    {

          if (!$id && isset($_POST['id']))
                  $id = intval($_POST['id']);

          if ($id)
          {
                 $model = Image::find($id);
                 if (!$model)
                    $this->setError('Изображения с таким идентификатором не существует');
                 else
                 {
                     if ($model->projects)
                     {
                         foreach ($model->projects as $project)
                             $project->update_attributes(array('preview' => null));
                     }
                     $model->delete();
                     return true;
                 }
          }
          else return false;
    }

    /*
     * админка
     * загрузчик для jQuery File Upload
     * модифицированный для сохранения инфы в сессию
     * см classes/fileupload.php
     */

    public function adminFileUpload()
    {
        $upload_handler = new CustomUploadHandler();
    }

    /*
     * админка
     * удаление картинки из галереи
     * ajax only
     */

    public function adminImageDelete()
    {
        $id = $this->getParam('id');
        if ($id)
        {
            $model = Image::find($id);
            if ($model)
            {
                if ($model->galleries)
                {
                    foreach($model->galleries as $rel)
                        $rel->delete();
                }
                if (is_file(dirname(__FILE__).'/../files/'.$model->name))
                    @unlink(dirname(__FILE__).'/../files/'.$model->name);

                $model->delete();
            }
        }
    }

    /*
     * админка
     * редактирование картинки из галереи
     * ajax only
     */

    public function adminImageEdit()
    {
        $id = $this->getParam('id');
        $description = $this->getParam('description');

        if ($id)
        {
            $model = Image::find($id);
            if ($model)
            {
                $model->update_attributes(array('description' => $description));
                echo $model->description;
            }
            else
                echo 'Ошибка, не удается сохранить изменения';
        }
    }

    /*
     * админка
     * сортировка картинок в галерее
     */
    public function adminImageSort($params)
    {
        if (!empty($_POST['item']))
        {
            foreach ($_POST['item'] as  $weight => $id)
            {
                $item = Gallery::find($id);
                $item->update_attributes(array('weight' => $weight));
            }
        }
    }
}
?>