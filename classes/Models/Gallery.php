<?php
class Gallery extends ActiveRecord\Model
{
    static $belongs_to = array(
        array('picture', 'class_name' => 'Image', 'foreign_key' => 'image', 'primary_key' => 'id'),
        array('projects', 'class_name' => 'Project', 'foreign_key' => 'project', 'primary_key' => 'id'),
    );
}