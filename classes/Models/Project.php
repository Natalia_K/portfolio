<?php
class Project extends ActiveRecord\Model
{
    const STATUS_ACTIVE = 1;
    const STATUS_CLOSED = 0;
    const STATUS_RESTRICTED = 2;

    static $validates_presence_of = array(
            array('front'),
            array('back'),
            array('description'),
            array('link'),
            array('status')
    );

    static $validates_inclusion_of = array(
             array('status', 'in' => array('0', '1', '2')),
    );

    static $has_many = array(
        array('galleries', 'foreign_key' => 'project', 'primary_key' => 'id', 'order' => 'weight asc'),
    );



    static $belongs_to = array(
        array('picture', 'class_name' => 'Image', 'foreign_key' => 'preview' )
    );

    public function statusLabel()
    {
        if ($this->status == self::STATUS_ACTIVE)
            return 'Действующий проект';
        elseif($this->status == self::STATUS_CLOSED)
            return 'Завершенный проект';
        elseif($this->status == self::STATUS_RESTRICTED)
            return 'Действующий проект';
        else
            return null;
    }

    public function viewLink()
    {
        if (!empty($this->link))
            return substr($this->link, strpos($this->link, '//')+2);
    }

    public function getFeatures()
    {
        if (!empty($this->features))
        {
            $features = explode(';', $this->features);
            $features = array_map('trim', $features);
            $features = array_filter($features, 'strlen');
            return $features;
        }

    }
}