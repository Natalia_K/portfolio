<?php
class Image extends ActiveRecord\Model
{
   static $has_many = array(
        array('galleries', 'primary_key' => 'id', 'foreign_key' => 'image'),
        array('projects', 'primary_key' => 'id', 'foreign_key' => 'preview'),
    );

}