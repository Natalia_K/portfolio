<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="ru" />
    <link rel="shortcut icon" href="<?php echo $app->baseUrl() ?>/favicon.ico?v=1" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="<?php echo $app->baseUrl() ?>/css/admin.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo $app->baseUrl() ?>/js/jQuery-File-Upload-9.8.0/css/jquery.fileupload.css" />
    <script type="text/javascript" src="<?php echo $app->baseUrl() ?>/js/jquery-1.11.1.min.js" ></script>
    <script type="text/javascript" src="<?php echo $app->baseUrl() ?>/js/ckeditor/ckeditor.js" ></script>
    <script type="text/javascript" src="<?php echo $app->baseUrl() ?>/js/jquery-ui-1.11.2/jquery-ui.min.js" ></script>
    <script src="<?php echo $app->baseUrl() ?>/js/jQuery-File-Upload-9.8.0/js/vendor/jquery.ui.widget.js"></script>
    <script src="<?php echo $app->baseUrl() ?>/js/jQuery-File-Upload-9.8.0/js/tmpl.min.js"></script>
    <script src="<?php echo $app->baseUrl() ?>/js/jQuery-File-Upload-9.8.0/js/load-image.all.min.js"></script>
    <script src="<?php echo $app->baseUrl() ?>/js/jQuery-File-Upload-9.8.0/js/canvas-to-blob.min.js"></script>
    <script src="<?php echo $app->baseUrl() ?>/js/jQuery-File-Upload-9.8.0/js/jquery.iframe-transport.js"></script>
    <script src="<?php echo $app->baseUrl() ?>/js/jQuery-File-Upload-9.8.0/js/jquery.fileupload.js"></script>
    <script src="<?php echo $app->baseUrl() ?>/js/jQuery-File-Upload-9.8.0/js/jquery.fileupload-process.js"></script>
    <script src="<?php echo $app->baseUrl() ?>/js/jQuery-File-Upload-9.8.0/js/jquery.fileupload-image.js"></script>
    <script src="<?php echo $app->baseUrl() ?>/js/jQuery-File-Upload-9.8.0/js/jquery.fileupload-audio.js"></script>
    <script src="<?php echo $app->baseUrl() ?>/js/jQuery-File-Upload-9.8.0/js/jquery.fileupload-video.js"></script>
    <script src="<?php echo $app->baseUrl() ?>/js/jQuery-File-Upload-9.8.0/js/jquery.fileupload-validate.js"></script>
    <script src="<?php echo $app->baseUrl() ?>/js/jQuery-File-Upload-9.8.0/js/jquery.fileupload-ui.js"></script>
    <script src="<?php echo $app->baseUrl() ?>/js/jquery.uploadfile.min.js"></script>
    <script type="text/javascript" src="<?php echo $app->baseUrl() ?>/js/admin-form.js" ></script>
</head>

<body>
<div class="wrapper">
    <header>
        <div class="container admin-area">
            <h1><?php echo ($model->is_new_record() ? 'Добавить проект' : 'Редактировать проект') ?></h1>
            <a href="<?php echo $app->baseUrl() ?>/logout" class="gtfo" title="выход"></a>
            <a href="<?php echo $app->baseUrl() ?>/admin/project/list" class="button return">&larr; проекты</a>

        </div>
    </header>
    <section>
        <div class="container admin-area">

            <form class="project-form" name="create-project" method="post" action="">
                <div class="row">
                    <label>Название проекта <span class="asterisc">*</span></label>
                    <input type="text" name="Project[name]" value="<?php echo $model->name ?>" />
                </div>
                <div class="row">
                    <label>Год создания</label>
                    <input type="text" name="Project[year]" value="<?php echo (!$model->is_new_record() ? $model->year : date('Y', time())) ?>"/>
                </div>
                <div class="row">
                    <label>Ссылка <span class="asterisc">*</span></label>
                    <input type="text" name="Project[link]" value="<?php echo $model->link ?>"/>
                </div>
                <div class="row">
                    <label>Роль в проекте</label>
                    <input type="text" name="Project[front]" placeholder="front-end" value="<?php echo $model->front ?>" class="inline"/>
                    <input type="text" name="Project[back]" placeholder="back-end" value="<?php echo $model->back ?>" class="inline"/>
                </div>
                <div class="row">
                    <label>Особенности</label>
                    <textarea name="Project[features]" class="paragraph"><?php echo $model->features ?></textarea>
                    <span class="note">Через точку с запятой (;)</span>
                </div>
                <div class="row">
                    <label>Описание</label>
                    <textarea  name="Project[description]" class="text" id="redactor"><?php echo $model->description ?></textarea>
                </div>
                <div class="row">
                <table>
                    <tbody>
                    <tr>
                        <td>
                            <label>Статус</label>
                            <select name="Project[status]">
                                <option value="">-------</option>
                                <option value="<?php echo Project::STATUS_ACTIVE ?>"<?php echo ($model->status == Project::STATUS_ACTIVE ? ' selected' : '')?>>Действующий</option>
                                <option value="<?php echo Project::STATUS_CLOSED ?>"<?php echo (!$model->is_new_record() && $model->status == Project::STATUS_CLOSED ? ' selected' : '')?>>Закрытый</option>
                                <option value="<?php echo Project::STATUS_RESTRICTED ?>"<?php echo (!$model->is_new_record() && $model->status == Project::STATUS_RESTRICTED ? ' selected' : '')?>>С ограниченным доступом</option>
                            </select>
                        </td>

                        <td>
                            <label>Превью</label>
                            <div id="preview-preview">
                                <?php if (!$model->is_new_record() && !empty($model->preview)) : ?>
                                    <p class="preview-preview-img"><img src="<?php echo $app->baseUrl() ?>/files/previews/<?php echo $model->picture->name ?>" width="100" /></p>
                                    <button id="delete-preview" class="button interface delete-preview" data-item="<?php echo $model->preview ?>">Удалить</button>
                                <?php endif; ?>
                            </div>
                            <div id="preview" class="button interface">Загрузить</div>
                            <input type="hidden"  name="Project[preview]" value="<?php echo $model->preview ?>" />

                        </td>
                    </tr>
                    </tbody>
                </table>
                 </div>

                <div class="row buttons">
                    <input type="submit" value="Сохранить" class="button" />
                </div>
            </form>
        <?php if(!$model->is_new_record() && count($model->galleries) > 0) : ?>
            <h3>Изображения</h3>
                <ul class="project-image-list" id="sortable">
                    <?php foreach($model->galleries as $image) : ?>
                         <li id="item-<?php echo $image->id ?>">
                                <p class="image-thumb"><img src="<?php echo $app->baseUrl() ?>/files/<?php echo $image->picture->name ?>"  width="100"/></p>
                                <p class="image-name"><?php echo $image->picture->name ?></p>
                                <p class="image-description" data-item="<?php echo $image->picture->id ?>"><?php echo $image->picture->description ?></p>
                                <a href="<?php echo $app->baseUrl() ?>/admin/image/delete/<?php echo $image->picture->id ?>" data-item="<?php echo $image->picture->id ?>" class="image-delete">&times;</a>
                                <div class="clearfix"></div>
                         </li>
                    <?php endforeach; ?>
                </ul>
        <?php endif; ?>

        <h3>Загрузить изображения</h3>
        <span class="btn btn-success fileinput-button">
                <span class="button interface upload">+ Загрузить</span>
                <input id="fileupload" type="file" name="files[]" multiple>
            </span>
        <br>
        <br>
        <div id="progress" class="progress">
            <div class="progress-bar progress-bar-success"></div>
        </div>
        <table id="files" class="files"></table>
    </section>
</div>
</body>
</html>