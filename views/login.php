<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="ru" />
    <link rel="shortcut icon" href="<?php echo $app->baseUrl() ?>/favicon.ico?v=1" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="<?php echo $app->baseUrl() ?>/css/layout.css" />
    <title><?php echo $app->name ?> - Вход</title>
</head>
<body>
<div class="wrapper">
    <header>
        <div class="container">
            <h1>
                <?php echo $app->name ?>
                <br/><small><?php echo $app->regalia ?></small>
            </h1>
            <ul class="contact">
                <li class="contact-item"><a href="mailto:<?php echo $app->mail ?>" class="contact-link mail"><i class="icon"></i><?php echo $app->mail ?></a></li>
                <li class="contact-item"><span class="contact-link phone"><i class="icon"></i><?php echo $app->phone ?></span></li>
                <li class="contact-item"><span class="contact-link skype"><i class="icon"></i><?php echo $app->skype ?></span></li>
            </ul>
        </div>
    </header>
    <section>
        <div class="container content">

            <form name="authentication" id="auth-form" class="authentication-form" method="post" action="<?php echo $app->baseUrl() ?>/login">
                <img src="<?php echo $app->baseUrl() ?>/images/noanonallowed.png" alt="no anonymous allowed" />
                <h3>Вход</h3>
                <div class="row">
                    <input type="text" name="login" placeholder="Логин" />
                </div>
                <div class="row">
                    <input type="password" name="password"  placeholder="Пароль"  />
                </div>
                <div class="row buttons">
                    <input type="submit" class="button" value="Войти" />
                </div>
            </form>
        </div>
    </section>
</div>
</body>
</html>

