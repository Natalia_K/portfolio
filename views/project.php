<div class="project-container">
    <a class="project-controls project-previous<?php echo $prev == $model->id ? ' disabled' : '' ?>" data-item="<?php echo $prev ?>" href=""></a>
    <a class="project-controls project-next<?php echo $next == $model->id ? ' disabled' : '' ?>" data-item="<?php echo $next ?>" href=""></a>
    <a href="/" class="project-close" id="hide-overlay"></a>
    <div class="project-full">
        <div class="project-heading">
            <h1><?php echo $model->name ?></h1>

            <div class="project-state">
                <?php if (!empty($model->link)) : ?>
                    <a href="<?php echo $model->link ?>" target="_blank" class="project-link"><?php echo $model->viewLink() ?></a>
                <?php endif; ?>
                <div class="project-state-info">
                    <div class="project-status state<?php echo $model->status ?>">
                        <?php if ($model->status == Project::STATUS_ACTIVE) : ?>
                            <i class="fa fa-heartbeat"></i>
                        <?php elseif($model->status == Project::STATUS_CLOSED) : ?>
                            <i class="fa fa-flag-checkered"></i>
                        <?php elseif($model->status == Project::STATUS_RESTRICTED) : ?>
                            <i class="fa fa-lock"></i>
                        <?php endif; ?>
                        <?php echo $model->statusLabel() ;?>
                    </div>
                    <?php if (!empty($model->year)) : ?>
                        <div class="project-year">Год создания <?php echo $model->year ?></div>
                    <?php endif; ?>
                </div>
            </div>

            <?php $features = $model->getFeatures(); ?>
            <div class="project-specific">
                <?php if (!empty($features)) :  ?>
                    <ul class="project-features">
                        <?php foreach ($features as $feature) : ?>
                            <li class="project-feature-item"><?php echo $feature; ?></li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>

                <?php if (!empty($model->front) || !empty($model->back)) : ?>
                    <div class="project-role">
                        Участие в проекте
                        <span><canvas id="front" width="50" height="50" data-value="<?php echo $model->front ?>"><?php echo $model->front ?>%</canvas><br>front end</span>
                        <span><canvas id="back" width="50" height="50" data-value="<?php echo $model->back ?>"><?php echo $model->back ?>%</canvas><br>back end</span>
                    </div>
                <?php endif; ?>
            </div>
        </div>

        <div class="project-body">
            <div class="project-details" id="project-details">

                <?php if (!empty($model->description)) : ?>
                    <div class="project-details-text">
                        <?php echo $model->description ?>
                    </div>
                <?php endif; ?>
            </div>
            <div class="project-gallery" id="project-gallery">
                <?php if ($model->galleries) : ?>
                    <ul class="project-images">
                        <?php  foreach($model->galleries as $image) : ?>
                            <li class="project-image-item">
                                <img src="<?php echo $app->baseUrl() ?>/files/<?php echo $image->picture->name ?>" alt="<?php echo $image->picture->name ?>" />
                                <?php if (!empty($image->picture->description)) : ?>
                                    <p class="project-image-description"><?php echo $image->picture->description ?></p>
                                <?php endif; ?>
                            </li>
                        <?php endforeach;  ?>
                    </ul>
                <?php endif; ?>
            </div>
        </div>
    </div>

</div>