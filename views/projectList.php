<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="ru" />
    <link rel="shortcut icon" href="<?php echo $app->baseUrl() ?>/favicon.ico?v=1" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="<?php echo $app->baseUrl() ?>/css/admin.css" />
    <script type="text/javascript" src="<?php echo $app->baseUrl() ?>/js/jquery-1.11.1.min.js" ></script>
    <script type="text/javascript" src="<?php echo $app->baseUrl() ?>/js/jquery-ui-1.11.2/jquery-ui.js" ></script>
    <script type="text/javascript" src="<?php echo $app->baseUrl() ?>/js/admin-list.js" ></script>
</head>

<body>
<div class="wrapper">
    <header>
        <div class="container admin-area">
            <h1>Проекты</h1>
            <a href="<?php echo $app->baseUrl() ?>/logout" class="gtfo" title="выход"></a>
        </div>
    </header>
    <section>
        <div class="container admin-area">
            <a href="<?php echo $app->baseUrl() ?>/admin/project/create" class="button add">+ Создать</a>
            <?php if (!empty($data)) : ?>
                <ul class="project-admin-list" id="sortable">
                    <?php foreach($data as $project) : ?>
                        <li id="item-<?php echo $project->id ?>">
                            <a href="<?php echo $app->baseUrl() ?>/admin/project/edit/<?php echo $project->id ?>">
                                <?php echo $project->name; ?>
                            </a>
                            <a href="<?php echo $app->baseUrl() ?>/admin/project/delete/<?php echo $project->id ?>" data-item="<?php echo $project->id ?>" class="project-delete">&times;</a>
                            <div class="clearfix"></div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php else : ?>
                <div class="no-content">Пока ни одного проекта нет</div>
            <?php endif; ?>

        </div>
    </section>
</div>
</body>
</html>