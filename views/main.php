﻿<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="ru" />
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo $app->baseUrl() ?>/favicon.ico?v=1" />
    <link rel="stylesheet" type="text/css" href="<?php echo $app->baseUrl() ?>/js/malihu-custom-scrollbar/jquery.mCustomScrollbar.css"" />
    <link href="<?php echo $app->baseUrl() ?>/js/jquery.bxslider.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo $app->baseUrl() ?>/css/layout.css" />
    <script type="text/javascript" src="<?php echo $app->baseUrl() ?>/js/jquery-1.11.1.min.js" ></script>
    <script type="text/javascript" src="<?php echo $app->baseUrl() ?>/js/malihu-custom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <script type="text/javascript" src="<?php echo $app->baseUrl() ?>/js/main.js" ></script>
    <script src="<?php echo $app->baseUrl() ?>/js/jquery.bxslider.min.js"></script>
    <title><?php echo $app->name ?> - <?php echo $app->regalia ?></title>
</head>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter44859409 = new Ya.Metrika({
                    id:44859409,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/44859409" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<body>
<div class="page">
    <div class="color"></div>
    <header>
        <div class="main">
            <div class="main-wrapper">
                <h1 class="head">
                    <?php echo $app->name ?>
                </h1>
                <ul class="contact">
                    <li class="contact-item"><a href="mailto:<?php echo $app->mail ?>" class="contact-link mail"><i class="icon mail"></i></a></li>
                    <li class="contact-item"><a href="skype:kt814a?chat"><i class="icon skype"></i></a></li>
                </ul>
                <div class="regalia">
                    <?php echo $app->regalia ?>
                </div>
                <div class="skills">
                    <?php echo $app->skills ?>
                </div>
            </div>
        </div>
    </header>

    <div class="display">
        <div class="screen">
            <div class="cam"></div>
            <?php if (!empty($data)) : ?>
                <div class="viewport-window"></div>
                <div class="viewport-overlay" id="viewport-overlay">
                    <a href="#" class="viewport-link" id="show-overlay"></a>
                </div>
            <?php endif; ?>
            <div class="viewport">
                <?php if (!empty($data)) : ?>
                    <?php foreach($data as $project) : ?>
                        <div class="viewport-slide" id="item-<?php echo $project->id ?>">
                            <img src="<?php echo $app->baseUrl() ?>/files/previews/<?php echo ($project->picture ? $project->picture->name : 'placeholder.png') ?>" alt="<?php echo $project->name ?>" />
                        </div>
                    <?php endforeach; ?>
                <?php else : ?>
                    <div class="no-content">Портфолио будет. Скоро.</div>
                <?php endif; ?>
            </div>
        </div>
        <div class="panel">

        </div>
        <div class="stand">
            <div class="stand-left"></div>
            <div class="stand-right"></div>
            <div class="plate"></div>
        </div>
    </div>

    <div class="list">
        <div class="list-wrapper">
            <?php if (!empty($data)) : ?>
            <ul class="list-list bx-slider">
                <?php foreach($data as $project) : ?>
                    <li class="list-item" data-item="<?php echo $project->id ?>" id="project-<?php echo $project->id ?>">
                        <a href="#" class="list-item-link"><?php echo $project->name ?></a>
                        <a href="" data-item="<?php echo $project->id ?>" class="list-item-details">подробнее</a>
                    </li>
                <?php endforeach; ?>
            </ul>
            <?php endif; ?>
            <ul class="list-controls">
                <li class="list-controls-prev"></li>
                <li class="list-controls-next"></li>
            </ul>
        </div>
    </div>
</div>
<footer>

</footer>
<div class="overlay" id="overlay"></div>
<script>



</script>
</body>
</html>
