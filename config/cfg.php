﻿<?php
return array(
    //настройки содержимого сайта
    'settings' => array(
        'name' => 'Наталья Крутова',
        'regalia' => 'web-разработчик',
        'skills' => 'php, HTML, JavaScript, Yii, Drupal',
        'phone' => '+7 921 5008117',
        'mail' => 'info@kt814a.ru',
        'skype' => 'kt814a'
    ),
    //доступ к админке
    'username' => 'admin',
    'password' => '21232f297a57a5a743894a0e4a801fc3',
    //база данных
    'db' => array(     //подключения к бд
        'development' => 'pgsql://postgres:root@127.0.0.1:5432/hramov',
        'production' => 'sqlite://./data/portfolio.db'
    ),
    'activeDbConnection' => 'production', //активное подключение
    //настройки сайта
    'baseUrl' => 'http://kt814a.ru',
    'baseDirectory' => '' //оставить пустым если сайт в корне

);