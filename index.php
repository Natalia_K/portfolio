<?php
require("classes/application.php");

$app = new Application;

$app->getUrlManager()->parseUrl();

$prefix = $app->getParam('target');
$action = $app->getParam('action');
$params = $app->getParam('params');

$app->callAction($action, $params, $prefix);

$errors = $app->getErrors();
if (!empty($errors))
var_dump($errors);

